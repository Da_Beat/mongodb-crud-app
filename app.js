const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const Joi = require('joi');
const db = require('./db');

const app = express();
// parses json data sent to us by the user 
app.use(bodyParser.json());

const port = 3000;
const coll = 'todo';

// schema used for data validation for our todo document
const schemaTodo = Joi.object().keys({
  todo : Joi.string().required()
});

// serve static html file to user
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// read
app.get('/getTodos', (req, res) => {
  db.getDB().collection(coll).find({}).toArray((err, documents) => {
    if (err) {
      console.log(err);
    } else {
      console.log(documents);
      res.json(documents);
    };
  });
});

// update
app.put('/:id', (req, res, next) => {
  const todoID = req.params.id;
  const userInput = req.body;

  Joi.validate(userInput, schemaTodo, (err, result) => {
    if (err) {
      const error = new Error("Empty Input");
      error.status = 400;
      next(error);
    } else {
      db.getDB().collection(coll).findOneAndUpdate(
        { _id: db.getPrimaryKey(todoID) }, 
        { $set : { todo: userInput.todo } }, 
        { returnOriginal: false },
        (err, result) => {
          if (err) {
            const error = new Error("Failed to insert Todo Document");
            error.status = 400;
            next(error);
          } else {
            res.json({
              result : result, 
              msg : "Successfully Updated Todo",
              error : null
            });
          };
        }
      );
    };
  });
});

// delete
app.delete('/:id', (req, res) => {
  const todoID = req.params.id;
  db.getDB().collection(coll).findOneAndDelete({ _id: db.getPrimaryKey(todoID) }, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.json(result);
    };
  });
});

// create 
app.post('/', (req, res, next) => {
  // document to be inserted
  const userInput = req.body;

  // validate document, if document is invalid pass to error middleware, else insert document within todo collection
  Joi.validate(userInput, schemaTodo, (err, result) => {
    if (err) {
      const error = new Error("Invalid Input");
      error.status = 400;
      next(error);
    } else {
      db.getDB().collection(coll).insertOne(userInput, (err ,result) => {
        if (err) {
          const error = new Error("Failed to insert Todo Document");
          error.status = 400;
          next(error);
        } else {
          res.json({
            result : result, 
            document : result.ops[0],
            msg : "Successfully inserted Todo",
            error : null
          });
        };
      });
    };
  });    
});

// middleware for handling Error, sends error response back to user
app.use((err, req, res, next)=>{
  res.status(err.status).json({
    error : {
      message : err.message
    }
  });
});

db.connect((err) => {
  // if err unable to connect to database, end application
  if (err) {
    console.log('Unable to connect to database');
    process.exit(1);
  } else {
    // successfully connected to database, start up our express application and listen for request
    app.listen(port, () => {
      console.log(`Connected to database,  app listening on port ${port}`);
    });
  };
});